<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 100 ; $i++) { 
            $order = factory(App\Order::class)->create();

            $products = App\Product::all()->random(rand(2, 5));
            foreach ($products as $product) {
                $id = $product->id;
                $price = $product->price;
                $quantity = rand(1, 50);
                // $product = App\Product::random(rand())
                $order->products()->attach($id, ['price' => $price, 'quantity' => $quantity]);
            }
        }
    }
}
