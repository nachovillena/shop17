<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Ratón',
            'code' => 'RA01',
            'price' => 10,
            'family_id' => 1,
        ]);
        DB::table('products')->insert([
            'name' => 'Ratón Multimedia',
            'code' => 'RA02',
            'price' => 12,
            'family_id' => 1,
        ]);
        factory(App\Product::class, 100)->create();
    }
}
