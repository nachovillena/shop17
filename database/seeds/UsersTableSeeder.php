<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'pepe',
            'role_id' => 1,
            'surname' => 'García López',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('pepe'),
        ]);
        DB::table('users')->insert([
            'name' => 'juan',
            'role_id' => 2,
            'surname' => 'Sánchez Moreno',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('juan'),
        ]);
    }
}
