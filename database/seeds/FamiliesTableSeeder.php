<?php

use Illuminate\Database\Seeder;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
            'code' => 'MICR',
            'name' => 'Microprocesadores',
        ]);
        DB::table('families')->insert([
            'code' => 'PERI',
            'name' => 'Perifericos',
        ]);
        DB::table('families')->insert([
            'code' => 'STOR',
            'name' => 'Almacenamiento',
        ]);
        DB::table('families')->insert([
            'code' => 'MEDI',
            'name' => 'Multimedia',
        ]);
    }
}
