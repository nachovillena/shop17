<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Product::class, function () {
    $faker = Faker\Factory::create('es_ES');
    return [
        'code' => str_random(4),
        'name' => 'article ' . $faker->word,
        'price' => rand(100, 10000) / 100,
        'family_id' => rand(1, 4)
    ];
});
$factory->define(App\Order::class, function () {
    $faker = Faker\Factory::create('es_ES');
    return [
        'user_id' => App\User::all()->where('role_id', '=', 2)->random()->id
        
    ];
});
