<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity', 'price');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function cost()
    {
        $cost = 0;
        foreach ($this->products as $product){
            $cost += $product->pivot->price * $product->pivot->quantity;
        }
        return $cost;
    }
}
