<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Mail;

class MailController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        echo 'enviamos mensaje.... <br>';
        Mail::send('emails.prueba', ['user' => $user], function ($message) use ($user) {
            $message->from('no-reply@shop17.com', 'Shop17');
            $message->to($user->email, $user->name)->subject('Your Reminder!');
        });
        return 'mensaje enviado';
    }
}