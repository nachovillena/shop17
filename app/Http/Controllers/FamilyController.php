<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;

class FamilyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        $family = new Family;
        $user = $request->user();
        if (! $user->can('view', $family)){

        }
        $families = Family::paginate(3);
        if ($request->ajax()) {
            return $families;
        } else {
            return view('family.index', ['families'=> $families]);
        }
    }
    public function ajax()
    {
        $families = Family::all();
            //return $families;
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('create', Family::class);
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
        'code' => 'required|unique:families',
        'name' => 'required|max:40'
        ]);

        $family = new Family($request->all());
        $family->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

        $families = Family::findOrFail($id);
        $this->authorize('view', $families);
        if ($request->ajax()) {
            return $families;
        } else {

            return view('family.show', ['family'=> $families]);

        }
    }
    public function showProducts($id, Request $request)
    {
        $families = Family::findOrFail($id);
        $products = $families->products()->orderBy('code')->paginate();
        if ($request->ajax()) {
            return $products;
        } else {

            return view('family.products', ['family'=> $families], ['products' => $products]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family = Family::findOrFail($id);
        $this->authorize('update', $family);
        return view('family.edit', ['family'=> $family]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
        'code' => 'required|unique:families,id: ' . $id . '|max:4',
        'name' => 'required|max:40'
        ]);
        $family = Family::find($id);
        $family->code = $request->code;
        $family->name = $request->name;
        $family->save();
        return redirect('/families');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Family::find($id)->delete();
        return redirect('/families');
    }
}
