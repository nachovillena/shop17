<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PruebasController extends Controller
{
    public function index($method)
    {
        $this->$method();
    }
    public function products()
    {
        echo DB::table('products')->toSql();
        $result = DB::table('products')->get();
        dd($result);
    }

    public function product()
    {
        echo DB::table('products')->toSql();
        $result = DB::table('products')->first();
        dd($result);
    }
    public function users()
    {
        echo DB::table('users')->toSql();
        $result = DB::table('users')->get();
        dd($result);
    }
    public function user1()
    {
        echo DB::table('users')->select('email')->toSql();
        $result = DB::table('users')->select('email')->first();
        dd($result);
    }
    public function families1()
    {
        echo DB::table('families')->select('code', 'name')->toSql();
        $result = DB::table('families')->select('code', 'name')->get();
        dd($result);
    }
    public function families2()
    {
        echo DB::table('families')->select('code', 'name')
                                  ->where('code', 'like', 'M%')
                                  ->toSql();
        $result = DB::table('families')->select('code', 'name')
                                       ->where('code', 'like', 'M%')
                                       ->get();
        dd($result);
    }
    public function families3()
    {
        echo DB::table('families')->select('code', 'name')
                                  ->where('code', 'like', 'M%')
                                  ->where('code', 'like', '%C%')
                                  ->toSql();
        $result = DB::table('families')->select('code', 'name')
                                       ->where('code', 'like', 'M%')
                                       ->where('code', 'like', '%C%')
                                       ->get();
        dd($result);
    }
    public function families4()
    {
        echo DB::table('families')->select('code', 'name')
                                  ->where('code', 'like', 'M%')
                                  ->orderBy('code', 'desc')
                                  ->toSql();
        $result = DB::table('families')->select('code', 'name')
                                       ->where('code', 'like', 'M%')
                                       ->orderBy('code', 'ascd')
                                       ->get();
        dd($result);
    }
    public function join2()
    {
        //toda la tabla 
        echo DB::tabla('families')->join('products', 'families.id', '=', 'products.family_id')->toSql();
        $result = DB::tabla('families')
                    ->join('products', 'families.id', '=', 'products.family_id')
                    ->get();
        dd($result);
    }
}
