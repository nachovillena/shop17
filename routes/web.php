<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//rutas para emails
Route::get('mail', function () {
    $user = Auth::user();
    echo 'enviamos mensaje.... <br>';
    Mail::send('emails.prueba', ['user' => $user], function ($message) use ($user) {
        $message->from('no-reply@shop17.com', 'Shop17');
        $message->to($user->email, $user->name)->subject('Your Reminder!');
    });
    return 'mensaje enviado';
});
    // Mail::send('vista', 'datos', function () {})
        // $message->from($address, $name = null);
        // $message->sender($address, $name = null);
        // $message->to($address, $name = null);
        // $message->cc($address, $name = null);
        // $message->bcc($address, $name = null);
        // $message->replyTo($address, $name = null);
        // $message->subject($subject);
        // $message->priority($level);
        // $message->attach($pathToFile, array $options = []);

Route::get('mail2', 'MailController@index');

Route::get('/home', 'HomeController@index');
Route::get('/casa', function () {
    return "pagina home";
})->middleware('admin');

Route::get('fluent/{method}', 'PruebasController@index');
Route::get('families/ajax', 'FamilyController@ajax');
Route::get('families/{id}/products', 'FamilyController@showProducts');
Route::get('/orders/products/{id}', 'OrderController@addProduct');
Route::get('sesion/{method?}/{key?}/{value?}', 'SessionController@index');
Route::resource('families', 'FamilyController');
Route::resource('products', 'ProductController');
Route::resource('orders', 'OrderController');
