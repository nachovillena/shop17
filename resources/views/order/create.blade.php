@extends('layouts.app')

@section('content')
    <h1>Pedido</h1>
    
    <table class="table table-bordered table-hover">
    <tr>
        <th>Producto</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Importe</th>
    </tr>
    <?php $cost = 0; ?>
    @foreach ($products as $product)
        <?php
        $elementCost = $product['price'] * $product['quantity'];
        $cost += $elementCost;
        ?>
        <tr>
        <td>
            {{ $product['product']->name }}
        </td> 
        <td>
            {{ number_format($product['price'], 2, "'", ".") }}€ 
            + 
            -
            borrar
        </td>
        <td>{{ $product['quantity'] }}</td> 
        <th>{{ number_format($elementCost, 2, "'", ".") }}€</th>
        </tr>
    @endforeach
    <tr>
        <th colspan="3">Total</th>
        <th>{{ number_format($cost, 2, "'", ".") }}€</th>
    </tr>
    </table>     
    <form action="/orders" method="post">
        {{ csrf_field() }}
        <input type="submit" value="Grabar">
    </form>

@endsection('content')