@extends('layouts.app')

@section('content')

<h1>Detalle de producto</h1>
<div class="form">
<form action="/products/{{ $product->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Código: </label>
        <input type="text" name="code" value="{{ old('code',  $product->code) }}">
        {{ $errors->first('code') }}
    </div>

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name',  $product->name) }}">
        {{ $errors->first('name') }}
    </div>

    <div class="form-group">
        <label>Precio: </label>
        <input type="text" name="price" value="{{ old('price',  $product->price) }}">
        {{ $errors->first('price') }}
    </div>

    <div class="form-group">
        <label>Familia: </label>
        <select type="select" name="family_id" value="{{ old('family_id',  $product->family_id) }}">
            @foreach ($families as $family)
            <option value="{{ $family->id }}">{{ $family->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('family_id') }}
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>
</form>
</div>

@endsection('content')