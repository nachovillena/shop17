@extends('layouts.app')

@section('content')
    <h1>Lista de productos</h1>
    @can('create', 'App\Product')
    <a href="/products/create">Nuevo</a>
    @else
            No puedes dar altas!!
    @endcan
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Familia</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
            <tr>
                <td>  {{ $product->id }} </td>
                <td>  {{ $product->code }} </td>
                <td>  {{ $product->name }} </td>
                <td>  {{ number_format($product->price, 2, "'", ".") }} €   </td>
                <td>  {{ $product->family->name }} </td>
                <td>  
                    <form method="post" action="/products/{{ $product->id }}">
                        @can('delete', $product)
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="submit" value="Borrar">
                        @endcan
                        <a href="/products/{{ $product->id }}/edit">Editar</a>
                        <a href="/products/{{ $product->id }}"> Ver </a>

                        <a href="/orders/products/{{ $product->id }}"> Comprar </a>
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $products->render() }} <br>
    {{ $products->links() }}

@endsection('content')