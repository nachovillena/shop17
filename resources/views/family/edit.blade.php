@extends('layouts.app')
    @section('content')
    <form class="form text-center" action="/families/{{ $family->id }}" method="post">
    {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
        <label>ID: </label><input type="text" name="id" value="{{ old('id', $family->id) }}" readonly>
        {{ $errors->first('id') }}
        <br>
        </div>
        <div class="form-group">
        <label>Codigo: </label><input type="text" name="code" value="{{ old('code', $family->code) }}">
        {{ $errors->first('code') }}
        <br>
        </div>
        <div class="form-group">
        <label>Nombre: </label><input type="text" name="name" value="{{ old('name', $family->name) }}">
        {{ $errors->first('name') }}
        <br>
        </div>
        <input type="submit" value="Guardar"><br>
    </form>

    @stop