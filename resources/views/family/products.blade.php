@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p> Detalle de familia: <b>{{ $family->name }}</b></p>
    </div>
        <table class="table"> 
        <tr>
            <th>id</th><th>Nombre</th><th>Precio</th><th>Codigo</th><th>Familia</th><th>Acción</th>
        </tr>
        @foreach($products as $product)
        <tr class="hover">
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $product['id'] }}</td>
            <td>{{ $product['name'] }}</td>
            <td>{{ $product['price'] }}€</td>
            <td>{{ $product['code'] }}</td>
            <td>{{ $family->name }}</td>

            <td>
                <form method="post" action="/products/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="Borrar">
                <a href="/products/{{ $product->id }}/edit">Editar</a>
                <a href="/products/{{ $product->id }}">Ver</a>
                </form>
            </td>
        </tr>
        @endforeach
        </table>

    @stop