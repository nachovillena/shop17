@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p>Lista de Familias</p>
    </div>
        <table class="table"> 
        <tr>
            <th>id</th><th>code</th><th>name</th><th>Acción</th>
        </tr>
        @foreach($families as $family)
        <tr class="hover">
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $family['id'] }}</td>
            <td>{{ $family['code'] }}</td>
            <td>{{ $family['name'] }}</td>
            <td>
                <form method="post" action="/families/{{ $family->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                @can('delete', $family)
                <input type="submit" value="Borrar">
                @endcan
                <a href="/families/{{ $family->id }}/edit">Editar</a>
                <a href="/families/{{ $family->id }}">Ver</a>
                </form>
            </td>
        </tr>
        @endforeach
        </table>
        @can('create', 'App\Family')
            <a href="/families/create" >Nuevo</a>
        @else
            No puedes dar altas!!
        @endcan
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#myModal">&times;</button>
        <h4 class="modal-title">Crear nueva Familia</h4>
      </div>
      <div class="modal-body">
        <form class="form text-center" action="/families" method="post">
        {{ csrf_field() }}
        <div class="form-group">
        <label>Codigo: </label><input type="text" name="code"><br>
        </div>
        <div class="form-group">
        <label>Nombre: </label><input type="text" name="name"><br>
        </div>
        <input type="submit" class="btn btn-default" value="Guardar"><br>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <div class="pagination">{!! $families->links() !!}</div>

    @stop