<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Informe de pedido</h1>
<h2>Cliente:  {{ $order->user->name }}</h2>
<h3>Fecha: {{ $order->updated_at }}</h3>
<table>
	<tr>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>Precio</th>
	</tr>
	<?php $total=0;?>
	@foreach($order->products as $product)
		<tr>
			<td>{{ $product->name }}</td>
			<td>{{ $product->pivot->quantity }}</td>
			<td>{{ $product->pivot->price }}€</td>
			<?php $total = $total + ($product->pivot->price * $product->pivot->quantity); ?>
		</tr>
	@endforeach
	<tr><td>Total:{{ $total }}€</td></tr>
</table>
</body>
</html>